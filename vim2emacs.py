#!/usr/bin/env python3
from collections import namedtuple
import re
import sys


let_colors_name_re = re.compile(r"^let g:colors_name = '([^']+)'$")
let_gui_color_re = re.compile(r"^let s:g(.) = '([0-9a-f]{6})'$")
cal_sid_h_re = re.compile(r"^cal <sid>h\('([^']+)'\s*,\s*(''|s:..)\s*,\s*(''|s:..)\s*,\s*(''|s:..)\s*,\s*(''|s:..)\s*,\s*'([^']+)'\s*,\s*(''|s:..)\s*\)$")

ColorAssignment = namedtuple('ColorAssignment', ['name', 'fg', 'bg', 'sp', 'attr'])
class VimTheme:
    def __init__(self):
        self.name = None
        self.base_colors = {}
        self.color_assignments = {}

def normalize_color(colorspec):
    if colorspec == "''":
        return None
    else:
        return colorspec[2:]
def extract_theme(input):
    theme = VimTheme()
    for line in input:
        m = let_colors_name_re.match(line)
        if m:
            theme.name = m.group(1)
            continue

        m = let_gui_color_re.match(line)
        if m:
            theme.base_colors[f"g{m.group(1)}"] = m.group(2)

        m = cal_sid_h_re.match(line)
        if m:
            assignment = ColorAssignment(
                name=m.group(1),
                fg=normalize_color(m.group(2)),
                bg=normalize_color(m.group(3)),
                sp=normalize_color(m.group(7)),
                attr=m.group(6),
            )
            theme.color_assignments[assignment.name] = assignment
        
        if '=== SET OPTIONS ===' in line:
            # todo
            break
            
    return theme

if __name__ == '__main__':
    theme = extract_theme(sys.stdin)
    print(theme.name)
    print(repr(theme.base_colors))
    print(repr(theme.color_assignments))


