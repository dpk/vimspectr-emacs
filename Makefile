.PHONY: all install clean

vimspectr_files = $(wildcard vimspectr/colors/*.vim)
emacs_theme_files = $(patsubst vimspectr/colors/%.vim,themes/%-theme.el,$(vimspectr_files))
all: vimspectr.el $(emacs_theme_files)

install: vimspectr.el $(emacs_theme_files) | ~/.emacs.d ~/.emacs.d/lisp ~/.emacs.d/vimspectr-themes
	cp vimspectr.el ~/.emacs.d/lisp
	cp $(emacs_theme_files) ~/.emacs.d/vimspectr-themes
~/.emacs.d/vimspectr-themes:
	mkdir -p $@

clean:
	rm -rf themes vimspectr.el

vimspectr.el: $(vimspectr_files) vimspectr.el.py vim2emacs.py
	printf '' > $@
	for definition in $(vimspectr_files); do ./vimspectr.el.py < $$definition >> $@; done
	echo '(provide '"'"'vimspectr)' >> $@

themes:
	mkdir -p themes
themes/%-theme.el: vimspectr/colors/%.vim vim2emacs.py vimspectr-x-theme.el.py | themes
	./vimspectr-x-theme.el.py < $< > $@
