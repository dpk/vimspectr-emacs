#!/usr/bin/env python3
import sys

import vim2emacs


if __name__ == '__main__':
    theme = vim2emacs.extract_theme(sys.stdin)
    for color_id, color_value in theme.base_colors.items():
        print(f'(setq {theme.name}-{color_id} "#{color_value}")')
