#!/usr/bin/env python3
import io
import sys

import vim2emacs


def emacs_face(ca, insist=False):
    global theme
    face = io.StringIO()
    face.write('((t ')
    if ca.fg:
        face.write(f':foreground ,{theme.name}-{ca.fg} ')
    if ca.bg:
        face.write(f':background ,{theme.name}-{ca.bg} ')
        
    if 'bold' in ca.attr:
        face.write(':weight bold ')
    elif insist:
        face.write(':weight normal ')

    if 'underline' in ca.attr:
        face.write(':underline t ')
    elif 'undercurl' in ca.attr:
        if ca.sp:
            face.write(f':underline (:color ,{theme.name}-{ca.sp} :style wave) ')
        else:
            face.write(f':underline (:color foreground-color :style wave) ')
    elif insist:
        face.write(':underline nil ')

    if 'italic' in ca.attr:
        face.write(':slant italic ')
    elif insist:
        face.write(':slant normal ')

    if 'reverse' in ca.attr:
        face.write(':inverse-video t ')
    elif insist:
        face.write(':inverse-video nil ')

    if insist:
        face.write(':box nil :strike-through nil :overline nil')

    face.write('))')
    face.seek(0)
    return face.read()

if __name__ == '__main__':
    global theme
    theme = vim2emacs.extract_theme(sys.stdin)
    name = theme.name
    ca = theme.color_assignments
    print(f"""
(deftheme {name}
  "The {name} colour scheme (by nightsense, ported to Emacs by dpk)")

(custom-theme-set-faces
  '{name}
  `(default {emacs_face(ca['Normal'])})
  `(cursor  {emacs_face(ca['Cursor'])})
  `(error   {emacs_face(ca['ErrorMsg'])})
  `(isearch {emacs_face(ca['Search'])})
  `(query-replace {emacs_face(ca['Search'])})
  `(highlight {emacs_face(ca['Search'])})
  `(lazy-highlight {emacs_face(ca['IncSearch'])})

  ;; no equivalent of button or link in vim
  `(button ((t :foreground ,{name}-gD :underline t)))
  `(link ((t :foreground ,{name}-g4 :underline t)))

  `(mode-line {emacs_face(ca['StatusLine'])})
  ;; no equivalent of minibuffer in vim so let's make something up
  `(minibuffer-prompt ((t :foreground ,{name}-gC)))

  `(fringe {emacs_face(ca['LineNr'], insist=True)})
  `(linum  {emacs_face(ca['LineNr'], insist=True)})
  `(line-number {emacs_face(ca['LineNr'], insist=True)})
  ;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Faces-for-Font-Lock.html#Faces-for-Font-Lock
  `(font-lock-warning-face       {emacs_face(ca['WarningMsg'])})
  `(font-lock-function-name-face {emacs_face(ca['Function'])})
  `(font-lock-variable-name-face {emacs_face(ca['Identifier'])})
  `(font-lock-keyword-face       {emacs_face(ca['Keyword'])})
  `(font-lock-builtin-face       {emacs_face(ca['Statement'])})
  `(font-lock-comment-face       {emacs_face(ca['Comment'])})
  `(font-lock-comment-delimiter-face {emacs_face(ca['Comment'])})
  `(font-lock-type-face          {emacs_face(ca['Type'])})
  `(font-lock-preprocessor-face  {emacs_face(ca['PreProc'])})
  `(font-lock-constant-face      {emacs_face(ca['Constant'])})
  `(font-lock-string-face        {emacs_face(ca['String'])})

  `(diff-added   {emacs_face(ca['DiffAdd'])})
  `(diff-removed {emacs_face(ca['DiffDelete'])})
  `(diff-changed {emacs_face(ca['DiffChange'])})
  `(diff-refine-changed {emacs_face(ca['DiffText'])})
  `(diff-refine-added   {emacs_face(ca['DiffText'])})
  `(diff-refine-removed {emacs_face(ca['DiffText'])})

  ;; contrast from pink for name to red for error isn't big enough with ErrorMsg
  `(rng-error     {emacs_face(ca['Error'])})

  `(flyspell-incorrect {emacs_face(ca['SpellBad'])})
  ;; SpellCap is actually 'miscapitalized word' in vim but emacs doesn't have that
  `(flyspell-duplicate {emacs_face(ca['SpellCap'])})

  `(show-paren-match    {emacs_face(ca['MatchParen'])})
  `(show-paren-mismatch {emacs_face(ca['Error'])})
)

(provide-theme '{name})
""")
