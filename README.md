# vimspectr for Emacs

This is a script to port the [vimspectr][] themes for Emacs by nightsense to Emacs. The build script requires Python 3.6 but has no other dependencies.

[vimspectr]: https://github.com/nightsense/vimspectr

To install, make sure you have `~/.emacs.d` and `~/.emacs.d/lisp` directories, and that the latter is in your `load-path`.

Run `git submodule init` and `git submodule update`, then `make install`.

Finally, add the following to your `~/.emacs.d/init.el`:

```elisp
(require 'vimspectr)
(add-to-list 'custom-theme-load-path  "~/.emacs.d/vimspectr-themes")
(load-theme 'vimspectrgrey-light t)
```

and replace `vimspectrgrey-light` with your chosen vimspectr theme.

(Yes, I know this is a bit of a faff. Sorry. May be on MELPA eventually.)

## TODO

- status line customization

